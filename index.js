var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var routes = [
   require('./routes/pizza'),
   require('./routes/order') 
]

app.use(bodyParser.json());
app.use('/', express.static('public'));
app.use('/', routes);

app.listen(8080, function () {
    console.log('Example app listening on http://localhost:8080!');
    console.log('The following routes are available:');
    console.log('GET /pizza');
    console.log('GET /pizza/:id');
    console.log('POST /order');
    console.log('/images/:filename');
});