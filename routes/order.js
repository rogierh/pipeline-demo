var express = require('express');
var router = express.Router();

router.route('/order')
    .post(function(req, res){
        switch(req.body.length) {
        case 1:
            console.log('CONGRATULATIONS! You just created a Pizza App and ordered one pizza')
            break;
        default:
            console.log('CONGRATULATIONS! You just created a Pizza App and ordered ' + req.body.length + ' delicious pizzas')
        };

        res.json({
            items: req.body,
            status: 'open' //put the status to open and return the modified order (additionally save this order to a database)
        });
    });

module.exports = router;