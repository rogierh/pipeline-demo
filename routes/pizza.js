var express = require('express');
var router = express.Router();

var pizzas = [{
    id: 1,
    name: "Americana",
    description: "Tomato sauce, mozzarella, pepperoni, ham & ground beef",
    price: 15,
    image: "americana"
}, {
    id: 2,
    name: "Four Seasons",
    description: "Tomato sauce, mozzarella, 1/4 pepperoni, 1/4 ham, 1/4 & mushrooms 1/4 paprika",
    price: 20,
    image: "four-seasons"
}, {
    id: 3,
    name: "Funghi",
    description: "Tomato sauce, mozzarella, shawarma & sachet garlic sauce",
    price: 10,
    image: "funghi"
}, {
    id: 4,
    name: "Margarita",
    description: "Tomato sauce, double mozzarella & oregano",
    price: 11,
    image: "margarita"
}];

router.route('/pizza')
    .get(function(req, res){
        res.json(pizzas);
    })
    .post(function(req, res){
        if(req.body.pizza) req.body.pizza.id = (pizzas.length + 1);
        pizzas.push(req.body.pizza);
        res.json(req.body.pizza);
    });

router.route('/pizza/:id')
    .get(function(req, res) {
        var result;
        for(var i=0; i < pizzas.length; i++){
            if(pizzas[i].id === parseInt(req.params.id)) result = pizzas[i];
        }
        res.json(result);
    })
    .put(function(req, res){
        for(var i=0; i < pizzas.length; i++){
            if(pizzas[i].id === parseInt(req.params.id)) pizzas[i] = req.body.pizza;
        }
        res.json(req.body.pizza);
    })
    .delete(function(req, res, next){
        for(var i=0; i < pizzas.length; i++){
            if(pizzas[i].id === parseInt(req.params.id)) pizzas.splice(i, 1);
        }
        res.send();
    });

module.exports = router;