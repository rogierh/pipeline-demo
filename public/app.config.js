angular.module("myPizzaApp")
.config(function($routeProvider){
		$routeProvider.
      		when('/detail/:id', {
        		templateUrl: 'templates/detail.html',
        		controller: 'detailController'
      		});
	})