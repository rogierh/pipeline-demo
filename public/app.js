(function(){
    //create here the module 'myApp'
    var app = angular.module("myPizzaApp", ["ngRoute"]);
    app.factory("shoppingCart", function(){
    	var shoppingCart = [];
    	return {
    		set: function(item){
    			shoppingCart.push(item);
    		}, 
    		get: function(){
    			return shoppingCart;
    		}
    	};
    });

	app.controller("myPizzaController", function($scope, $http, shoppingCart) {
	    $http.get('/pizza').then(function(response) {
            $scope.pizzas = response.data;
        });
        $scope.addToCart = function(item){
        	shoppingCart.set(item);
        }
        $scope.getCart = shoppingCart.get();
	});

	app.controller("detailController", function($scope, $http, $routeParams){
		$http.get('/pizza/' + $routeParams.id).then(function(response){
			$scope.pizza = response.data;
		});
	});

	app.controller("cartController", function($scope, shoppingCart){
		$scope.orders = shoppingCart.get();
		$scope.name = "";
	});

})();